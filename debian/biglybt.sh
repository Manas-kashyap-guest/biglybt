#!/bin/sh

JAVA_ARGS=$(grep -o '^[^#]*' ~/.biglybt/java.vmoptions 2>/dev/null | tr '\n' ' ')

java -cp /usr/share/java/biglybt-core.jar:/usr/share/java/biglybt-ui.jar:/usr/share/java/commons-cli.jar:/usr/share/java/swt4.jar:/usr/share/java/bcprov.jar \
     ${JAVA_ARGS} \
     -Dazureus.install.path=/usr/share/biglybt \
     com.biglybt.ui.Main "$@"
